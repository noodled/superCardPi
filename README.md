# SuperCardPi
Design and stuff for my Raspberry Pi based Retropie portable.

I designed this to be fully compatible with snes games, so it has the d-pad,
action buttons and shoulder buttons. I didn't add joysticks, so no n64 and PlayStation


# Hardware

### Parts list:

* A raspberry pi (I used a pi zero W).

* A micro-HDMI to HDMI adapter (for the pi zero).

* A heatsink for the Pi (this thing needs it, it reaches 50 degrees C while running
  super metroid)

* A battery bank with 2 USB outputs (so I don't have to make my own power circuitry),
  I'm estimating it'll run for  6 to 7 hours on a 10000mah battery.

* An SD card (use one that is 8 gigabytes or higher to store all those games).

* A 5 inch HDMI TFT-LCD, I'm using [this one from waveshare.](https://www.waveshare.com/5inch-hdmi-lcd.htm)

* 10 push buttons, use good ones, I used cheap ones I had lying around and they
  barely lasted 1 hour of use before wearing out, so get good ones.

* 2 shoulder buttons from an old PlayStation controller.

* The D-pad and action button caps from the same playstation controller.

* Corrugated cardboard, the best material to prototype with, I want to make the
  final one with laser cut plastic.

* Various screws, bolts, wires, and hot-glue.

# Circuit Diagrams and Wiring

Wire up the buttons to the Raspberry pi using the diagram and table below as reference.
I'm wiring the buttons so they pull their pins to ground when pressed.

![pinout diagram](https://raw.githubusercontent.com/afshaan4/Pi_switch/master/Pictures/gaming_pin-legend.png)
>Pin diagram derived from [Adafruit's documentation](https://learn.adafruit.com/super-game-pi/circuit-diagram#raspberry-pi-gpio)

Raspberry pi       |       Buttons
-------------------|-------------------
GPIO 17, Pin 11    |       UP
GPIO 27, Pin 13    |       DOWN
GPIO 22, Pin 15    |       LEFT
GPIO 23, Pin 16    |       RIGHT
GPIO 18, Pin 12    |       Select
GPIO 04, Pin 07    |       Start
GPIO 24, Pin 18    |       A Button
GPIO 10, Pin 19    |       B Button
GPIO 09, Pin 21    |       X Button
GPIO 25, Pin 22    |       Y Button
GPIO 11, Pin 23    |       Left shoulder button (L)
GPIO 08, Pin 24    |       Right shoulder button (R)

Remember: use good buttons, or they'll wear out really fast and stop working.

# Software Setup

I'm running [retropie](https://retropie.org.uk/) on the pi.

And I'm using [Adafruit-Retrogame](https://github.com/adafruit/Adafruit-Retrogame) to map the buttons to a virtual keyboard.

* Install Retropie on the raspberry pi: https://retropie.org.uk/docs/First-Installation/
  I suggest you read through the *First Installation* and *Getting Started* sections of the documentation linked above,
  it would make understanding later steps easier.

* Before plugging the SD card you installed Retropie on into the Raspberry pi,
  add the following lines to config.txt in the *boot* folder of the SD card:

  ```
  hdmi_force_hotplug=1
  hdmi_group=2
  hdmi_mode=1
  hdmi_mode=87
  hdmi_cvt 800 480 60 6 0 0 0
  overscan=1
  ```

  This is done to set the resolution of the display, so the picture fits the screen.
  If you use a different display change these setting to fit your screen, this
  page is a great reference it you are changing these settings: https://www.raspberrypi.org/documentation/configuration/config-txt/video.md


* When you first boot retropie, it will ask you to set up your game controllers,
  when it does that choose to set up using a keyboard, use the first Installation guide for reference, and
  set the controller inputs to these keys on a keyboard:

  Keyboard       |      Controller
  ---------------|--------------------
  left arrow     |      D-pad left
  right arrow    |      D-pad right
  up arrow       |      D-pad up
  down arrow     |      D-pad down
  Z              |      'A' button
  X              |      'B' button
  S              |      'X' button
  A              |      'Y' button
  ESC            |      'Select' button
  ENTER          |      'Start' button
  Q              |      Left shoulder button
  W              |      Right shoulder button

  I'm using the key mapping for Adafruit's [Super game pi](https://learn.adafruit.com/super-game-pi/overview) since mine is pretty much the same.


* For audio I'm using a rando USB audio card, so set the pi to use the USB audio
  card for audio out, I used the instructions from this guide: https://learn.adafruit.com/usb-audio-cards-with-a-raspberry-pi/instructions


* Then install Adafruit-Retrogame to map the buttons connected to the pi's GPIO
  to the keys you assigned earlier:

  ```
  #make sure you're in the home folder
  cd

  #download it
  curl -O https://raw.githubusercontent.com/adafruit/Raspberry-Pi-Installer-Scripts/master/retrogame.sh

  #run it as superuser
  sudo bash retrogame.sh
  ```

  Once you run it from the menu presented to you, select "Super game pi",
  once it's done it'll ask you if you want to reboot, select yes and reboot it.


# First Test:

![test](https://raw.githubusercontent.com/afshaan4/Pi_switch/master/Pictures/running.jpg)

**Do this before assembling it so you don't have to find that one loose connection after gluing it all together.**

* Transfer your roms for the games you want to play over to the pi using [this guide.](https://retropie.org.uk/docs/Transferring-Roms/)

* Then connect the buttons if you haven't already and connect it to the screen.

* Turn it on and see if you screen is working, then test the buttons and make sure they work.


# Final Assembly:

#### Cut the cardboard front and back panels:
The dimensions of the cardboard panels are 10.5 by 22.5 centimeters.

![panels](https://raw.githubusercontent.com/afshaan4/Pi_switch/master/Pictures/panels.jpg)

#### Add the cardboard standoffs for the buttons like so:

The standoffs are 2 centimeters tall and fit right under the edges of the button boards.

![standoffs](https://raw.githubusercontent.com/afshaan4/Pi_switch/master/Pictures/buttonStandoffs.jpg)

#### Then hot glue the button boards to the standoffs:

![Buttons](https://raw.githubusercontent.com/afshaan4/Pi_switch/master/Pictures/workingbuttons.jpg)

#### Add the shoulder buttons and their supports:

![shoulderButtons](https://raw.githubusercontent.com/afshaan4/Pi_switch/master/Pictures/shoulderButtons.jpg)

#### Then cut the holes for the D-pad and ABXY buttons:

![actionButtons](https://raw.githubusercontent.com/afshaan4/Pi_switch/master/Pictures/buttons.jpg)

#### Then glue the side wall onto the back panel:

![sideWalls](https://raw.githubusercontent.com/afshaan4/Pi_switch/master/Pictures/chassis.jpg)

#### Cut holes into the side walls for cooling:

I'm passively cooling the Pi with a heatsink so we need ventilation.
Also make sure to rout wires out of the way so they don't obstruct airflow.
![ventilation](https://raw.githubusercontent.com/afshaan4/Pi_switch/master/Pictures/vents.jpg)

#### Wire up the buttons to the Pi, and connect the screen to the Pi, then screw the front plate on:

Cut a hole in the back so you can snake out the power cables to the battery,
I'm mounting the battery on the back.
![finish](https://raw.githubusercontent.com/afshaan4/Pi_switch/master/Pictures/finished.jpg)
And you're done!

# Licensing stuff.

All the documentation in this repository is licensed under the MIT license which
you should have received with this, to see the exact terms of the license read the "LICENSE" file, if you didn't receive the license you can view it
[here](https://github.com/afshaan4/SuperCardPi/blob/master/LICENSE)

All images and diagrams in this repository is licensed under the
**Creative Commons Attribution-ShareAlike 4.0 International** license, to view
the exact terms visit: https://creativecommons.org/licenses/by-sa/4.0/legalcode

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.
